# 6. Credential Proving Flow

The high level overview of Credential proving steps:

1. Download and Parse received Proof Request message
2. Create Proof state object using parsed Proof Request message
    1. Serialize Proof state object and save serialized representation
    1. Update message (connected to Proof Request) status on the Agent as read
3. Retrieve and select credentials for Proof Request filling
4. Share Proof
    1. Deserialize associated Connection state object
    1. Deserialize Proof state object
    1. Generate and Send Proof message
5. Reject Proof Request
    1. Deserialize associated Connection state object
    1. Deserialize Proof state object
    1. Send Proof Reject message

> **NOTE:** library should be initialized before using proofs API. See [initialization documentation](2.Initialization.md)

Proof Request is a request sent by one of the communication sides to another one in order to get information about it.
It can contain two sorts of data (Proof Request can contain only one of them or both at the same time):
* Requested Attributes - attributes for which the sender wants to know the exact values.
* Requested Predicates - conditions that the sender wants to ensure that receiver has a credential containing attribute resolving them.

## 1. Get Proof Request offer from pending messages

See [messages documentation](8.Messages.md) for message download information.
Pending messages with proof request type should be downloaded.


1. Download pending messages
See [messages documentation](8.Messages.md) for message downloading information. Pending messages with `proof request` type should be downloaded.

2. Parse pending message

    Extract proof request JSON string from message (see [sample](#proof-request-sample)).
    
    For code example, see [ProofRequestsViewModel#checkProofRequests()](/examples/android/CMeSdkJava/app/src/main/java/me/connect/sdk/java/sample/proofs/ProofRequestsViewModel.java#L106)

## 2. Create Proof state object using parsed Proof Request message

1. Create Proof state object with received Proof Request 

    ### iOS
    ```objc
    [appDelegate.sdkApi proofCreateWithRequest: sourceId
            withProofRequest: message
            completion:^(NSError *error, vcx_proof_handle_t credentailHandle) {
                // ...
            }];
    ```
    
    ### Android
    
    ```java
    int proofHandle = DisclosedProofApi.proofCreateWithRequest(sourceId, message).get();
    ```
    
    * `sourceId` - random string
    * `message` - message received on step 1

1. Serialize Proof state object

    ### iOS
    ```objc
    [appDelegate.sdkApi proofSerialize: proofHandle
            completion:^(NSError *error, NSString *proof_request) {
                // ...
            }];
    ```
    
    ### Android
    
    ```java
    String serializedProof = DisclosedProofApi.proofSerialize(proofHandle).get();
    ```

1. Store serialized Proof for latter operations.

1. Update message status. See [messages documentation](8.Messages.md) for message update information.

## 3. Retrieve and select credentials for Proof Request filling

1. Deserialize Proof state object associated with Proof Request

    #### iOS
    ```objC
    [appDelegate.sdkApi proofDeserialize:serializedProof
            completion:^(NSError *error, NSInteger proofHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int proofHandle = ProofApi.proofDeserialize(serializedProof).get();
    ```

1. Retrieve proof-matching credentials

    Proof request fields should be filled with available credentials.
    
    ### iOS
    ```objc
    [appDelegate.sdkApi proofDeserialize: serializedProof
            completion:^(NSError *error, vcx_proof_handle_t proofHandle) {
                // ...
                [appDelegate.sdkApi proofRetrieveCredentials: proofHandle
                    withCompletion:^(NSError *error, NSString *retrievedCreds) {
                        // ...                    
                    }];
            }];
    ```
    
    ### Android
    
    ```java
    int proofHandle = DisclosedProofApi.proofDeserialize(serializedProof).get();
    String retrievedCreds = DisclosedProofApi.proofRetrieveCredentials(proofHandle).get();
    ```
    
    `retrievedCreds` field will contain JSON object with available credentials ([see sample](#retrieved-credentials-sample))
    
    JSON with selected attributes should be prepared.
    
    See [Proofs#mapCredentials()](/examples/android/CMeSdkJava/lib/src/main/java/me/connect/sdk/java/Proofs.java) for sample. Note that this method automatically takes first possible credential from available.
    
    --- 

1. Fill in missing attributes which can be self attested by user (according to Proof Request).

    Proof could contain self-attested attributes list. 
    In this case JSON object with user-provided inputs should be constructed.
    
    > **NOTE:** In case self-attested attributes are not required, empty JSON array should be used: `{}`  

1. If user does not have a credential to fill in missing attributes and these attributes cannot be self attested (according to Proof Request) you will not be able to generate Proof. So you can only reject received Proof Request.

## 4. Share Proof

In case user is able to fulfill received proof request, following steps should be performed to share proof.

1. Deserialize Connection state object associated with received Prof Request message

    #### iOS
    ```objC
    [appDelegate.sdkApi connectionDeserialize:serializedConnection
            completion:^(NSError *error, NSInteger connectionHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int connectionHandle = ConnectionApi.connectionDeserialize(serializedConnection).get();
    ```

1. Deserialize Proof state object associated with Proof Request

    #### iOS
    ```objC
    [appDelegate.sdkApi proofDeserialize:serializedProof
            completion:^(NSError *error, NSInteger proofHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int proofHandle = DisclosedProofApi.proofDeserialize(serializedProof).get();
    ```
    
1. Generate Proof using selected credentials and self attested attributes

    ### iOS
    ```objc
    [appDelegate.sdkApi proofGenerate: proofHandle
            withSelectedCredentials:selectedCredentials
            withSelfAttestedAttrs:selfAttestedAttributes
            completion:^(NSError *error) {
                // ...
            }];
    ```
    
    ### Android
    
    ```java
    DisclosedProofApi.proofGenerate(proofHandle, selectedCredentials, selfAttestedAttributes).get();
    ```
 
1. Send Proof to requester

    ### iOS
    ```objc
    [appDelegate.sdkApi proofSend: proofHandle
            withConnectionHandle:connectionHandle
            completion:^(NSError *error) {
                // ...
            }];
    ```
    
    ### Android
    
    ```java
    DisclosedProofApi.proofSend(proofHandle, connectionHandle).get();
    ```

1. Await proof status change

    Await proof status change. Call following code in loop until returned state is not equal `4` (`Accepted`):
    
    #### iOS
    ```objc
    while(1) {
        [appDelegate.sdkApi proofUpdateState:handle            
                completion:^(NSError *error, NSInteger pstate)) {
                    [appDelegate.sdkApi proofGetState:handle            
                        completion:^(NSError *error, NSInteger state)) {
                            if (state == 4){
                                break;
                            }
                        }];
                }];
    }
    ```
    
    #### Android
    ```java
    int status = -1
    while (status != 4) {
        DisclosedProofApi.proofUpdateState(handle).get();
        state = DisclosedProofApi.proofGetState(handle).get();
    }
    ```


## 4. Reject Proof Request

1. Deserialize Connection state object associated with received Prof Request message

    #### iOS
    ```objC
    [appDelegate.sdkApi connectionDeserialize:serializedConnection
            completion:^(NSError *error, NSInteger connectionHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int connectionHandle = ConnectionApi.connectionDeserialize(serializedConnection).get();
    ```

1. Deserialize Proof state object associated with Proof Request

    #### iOS
    ```objC
    [appDelegate.sdkApi proofDeserialize:serializedProof
            completion:^(NSError *error, NSInteger proofHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int proofHandle = DisclosedProofApi.proofDeserialize(serializedProof).get();
    ```

3. Reject Proof Request

    #### iOS
    ```objc
    [appDelegate.sdkApi proofDeclinePresentationRequest: proofHandle
            withConnectionHandle:connectionHandle
                      withReason:(NSString *)@"Rejection reason"
                    withProposal:(NSString *)nil
            withCompletion:^(NSError *error) {                        
                // ...
            }];
    ```
    
    #### Android
    ```java
    DisclosedProofApi.proofDeclineRequest(proofHandle, connectionHandle, "Rejection reason", null).get();
    ```

1. Await proof status change

    Await proof status change. Call following code in loop until returned state is not equal `9` (`Rejected`):
    
    #### iOS
    ```objc
    while(1) {
        [appDelegate.sdkApi proofUpdateState:handle            
                completion:^(NSError *error, NSInteger pstate)) {
                    [appDelegate.sdkApi proofGetState:handle            
                        completion:^(NSError *error, NSInteger state)) {
                            if (state == 9){
                                break;
                            }
                        }];
                }];
    }
    ```
    
    #### Android
    ```java
    int status = -1
    while (status != 9) {
        DisclosedProofApi.proofUpdateState(handle).get();
        state = DisclosedProofApi.proofGetState(handle).get();
    }
    ```

## Requested Attributes

Requested Attributes are fields that the sender wants to know exact values. Proof provided by another side will contain exact value and math evidence for a requested attribute.

## Requested Predicates

Requested Predicates are conditions that the sender wants to ensure that receiver has a credential containing attribute which can resolve this condition but the receiver can avoid revealing an exact value.
Proof provided by another side will contain math evidence of the fact that he has a credential solving predicate condition, but there will not be shown an exact value used for solving this predicate. 

Predicates can be requested only over fields which has integer representation in a credential (e.x. predicate can be requested for credential field `age` with has value `20` but not `twenty`)

There are 4 types of supported predicates
* '>=' - greater or equal
* '>' - greater
* '<=' - less or equal
* '<' - less

Note: Credentials do not specify units of measurement. 
There is a possible situation when the requested predicate, in theory, is resolvable but no credentials are returned by a library.
Example: credentials contains `height` with value `2` (implies maters) but requested predicate contains `p_value` as `170` (implies centimeters). 
For this case, it will be impossible to fulfill the proof request.

## Proof request message sample

```json
{
    "@type": {
        "name": "PROOF_REQUEST",
        "ver": "1.0",
        "fmt": "json"
    },
    "@msg": "{\"@type\":{\"name\":\"PROOF_REQUEST\",\"version\":\"1.0\"},\"@topic\":{\"mid\":0,\"tid\":0},\"proof_request_data\":{\"nonce\":\"220867029780621153091790\",\"name\":\"Basic Info\",\"version\":\"0.1\",\"requested_attributes\":{\"Number\":{\"name\":\"Number\"},\"First Name\":{\"name\":\"First Name\"},\"Last Name\":{\"name\":\"Last Name\"},\"Color\":{\"name\":\"Color\"},\"Job Title\":{\"name\":\"Job Title\"}},\"requested_predicates\":{},\"non_revoked\":null},\"msg_ref_id\":null,\"from_timestamp\":null,\"to_timestamp\":null,\"thread_id\":null}"
}
```

## Proof request sample
* `name` - human-readable name explaining requesting data
* `requested_attributes` - the list of requested attributes in the format
    ```
    {
        <unique_key_0>: {
            "name": <requested_field_1>
        },
        <unique_key_1>: {
            "name": <requested_field_1>
        },
        ....
    }
    ```
* `requested_predicates` - the list of requested attributes in the format
    ```
    {
        <unique_key_2>: {
            "name": <requested_predicate_field_1>,
            "p_type": <predicate_type>,
            "p_value": <threashhold as int>
        },
        <unique_key_3>: {
            "name": <requested_predicate_field_1>,
            "p_type": <predicate_type>,
            "p_value": <threashhold as int>
        },
        ....
    }
    ```

```json
{
    "@type": {
        "name": "PROOF_REQUEST",
        "version": "1.0"
    },
    "@topic": {
        "mid": 0,
        "tid": 0
    },
    "proof_request_data": {
        "nonce": "220867029780621153091790",
        "name": "Basic Info",
        "version": "0.1",
        "requested_attributes": {
            "attribute_1": {
                "name": "Number"
            },
            "attribute_2": {
                "name": "First Name"
            },
            "attribute_3": {
                "name": "Last Name"
            }
        },
        "requested_predicates": {
            "predicate_1": {
                "name": "Age",
                "p_type": ">=",
                "p_value": 20
            }
        },
        "non_revoked": null
    },
    "msg_ref_id": null,
    "from_timestamp": null,
    "to_timestamp": null,
    "thread_id": null
}
```

Following fields could be used for user interaction:

* `proof_request_data[request_attributes]` - list of attributes requested for proof
* `proof_request_data[requested_predicates]` - list of predicates requested for proof


## Retrieved credentials sample 

Retrieved credentials contains a list of credentials which can be used for proving of particular requested attribute and requested predicate.
Note that both (attributes and predicates) will be put under the same `attrs` field.

```json
{
    "attrs": {
        "attribute_1": [
            {
                "cred_info": {
                    "attrs": {
                        "Number": "12345",
                        "First Name": "Alice",
                        "Last Name": "Andersen",
                        "Age": "22"
                    },
                    "cred_def_id": "R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1",
                    "cred_rev_id": null,
                    "referent": "79f68e11-0aaf-4891-8277-1f90f5b05670",
                    "rev_reg_id": null,
                    "schema_id": "R9kuPrDFDVKNRL1oFpRxg2:2:DEMO-Transcript:1.0"
                },
                "interval": null
            }
        ],
        "attribute_2": [
            {
                "cred_info": {
                    "attrs": {
                        "Number": "12345",
                        "First Name": "Alice",
                        "Last Name": "Andersen",
                        "Age": "22"
                    },
                    "cred_def_id": "R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1",
                    "cred_rev_id": null,
                    "referent": "79f68e11-0aaf-4891-8277-1f90f5b05670",
                    "rev_reg_id": null,
                    "schema_id": "R9kuPrDFDVKNRL1oFpRxg2:2:DEMO-Transcript:1.0"
                },
                "interval": null
            }
        ],
        "attribute_3": [
            {
                "cred_info": {
                    "attrs": {
                        "Number": "12345",
                        "First Name": "Alice",
                        "Last Name": "Andersen",
                        "Age": "22"
                    },
                    "cred_def_id": "R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1",
                    "cred_rev_id": null,
                    "referent": "79f68e11-0aaf-4891-8277-1f90f5b05670",
                    "rev_reg_id": null,
                    "schema_id": "R9kuPrDFDVKNRL1oFpRxg2:2:DEMO-Transcript:1.0"
                },
                "interval": null
            }
        ],
        "predicate_1": [
            {
                "cred_info": {
                    "attrs": {
                        "Number": "12345",
                        "First Name": "Alice Andersen",
                        "Last Name": "Andersen",
                        "Age": "22"
                    },
                    "cred_def_id": "R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1",
                    "cred_rev_id": null,
                    "referent": "79f68e11-0aaf-4891-8277-1f90f5b05670",
                    "rev_reg_id": null,
                    "schema_id": "R9kuPrDFDVKNRL1oFpRxg2:2:DEMO-Transcript:1.0"
                },
                "interval": null
            }
        ]
    }
}
```

> **NOTE**: In case any of the fields contain empty array, that means that no credentials available and accepting process cannot be proceeded further.

## Selected Credentials sample 

This is JSON containing credentials selected by a user to use for proving of requested attributes and predicates.

```json
{
    "attrs": {
        "attribute_1": {
            "credential": {
                "cred_info": {
                    "attrs": {
                        "Number": "12345",
                        "First Name": "Alice",
                        "Last Name": "Andersen",
                        "Age": "22"
                    },
                    "cred_def_id": "R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1",
                    "cred_rev_id": null,
                    "referent": "79f68e11-0aaf-4891-8277-1f90f5b05670",
                    "rev_reg_id": null,
                    "schema_id": "R9kuPrDFDVKNRL1oFpRxg2:2:DEMO-Transcript:1.0"
                },
                "interval": null
            }
        },
        "attribute_2": {
            "credential": {
                "cred_info": {
                    "attrs": {
                        "Number": "12345",
                        "First Name": "Alice",
                        "Last Name": "Andersen",
                        "Age": "22"
                    },
                    "cred_def_id": "R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1",
                    "cred_rev_id": null,
                    "referent": "79f68e11-0aaf-4891-8277-1f90f5b05670",
                    "rev_reg_id": null,
                    "schema_id": "R9kuPrDFDVKNRL1oFpRxg2:2:DEMO-Transcript:1.0"
                },
                "interval": null
            }
        },
        "attribute_3": {,
            "credential": {
                "cred_info": {
                    "attrs": {
                        "Number": "12345",
                        "First Name": "Alice",
                        "Last Name": "Andersen",
                        "Age": "22"
                    },
                    "cred_def_id": "R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1",
                    "cred_rev_id": null,
                    "referent": "79f68e11-0aaf-4891-8277-1f90f5b05670",
                    "rev_reg_id": null,
                    "schema_id": "R9kuPrDFDVKNRL1oFpRxg2:2:DEMO-Transcript:1.0"
                },
                "interval": null
            }
        },
        "predicate_1": {
            "credential": {
                "cred_info": {
                    "attrs": {
                        "Number": "12345",
                        "First Name": "Alice",
                        "Last Name": "Andersen",
                        "Age": "22"
                    },
                    "cred_def_id": "R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1",
                    "cred_rev_id": null,
                    "referent": "79f68e11-0aaf-4891-8277-1f90f5b05670",
                    "rev_reg_id": null,
                    "schema_id": "R9kuPrDFDVKNRL1oFpRxg2:2:DEMO-Transcript:1.0"
                },
                "interval": null
            }
        }
    }
}
```