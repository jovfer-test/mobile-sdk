### Helpful links

- [https://github.com/evernym/mobile-sdk/tree/master/examples](https://github.com/evernym/mobile-sdk/tree/master/examples) - Examples for iOS and Android steps with

- [Push notifications iOS](https://developer.apple.com/documentation/usernotifications)

- [Push notifications Android](https://developer.android.com/guide/topics/ui/notifiers/notifications) 

- [Push notifications Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging/android/client) 


