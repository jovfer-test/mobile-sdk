//
//  ProductionPoolTxnGenesis.h
//  CMeSdkObjc
//
//  Created by Predrag Jevtic on 28/05/2020.
//  Copyright © 2020 Evernym Inc. All rights reserved.
//

#ifndef ProductionPoolTxnGenesis_h
#define ProductionPoolTxnGenesis_h

extern NSString * const productionPoolTxnGenesisDef;

#endif /* ProductionPoolTxnGenesis_h */
