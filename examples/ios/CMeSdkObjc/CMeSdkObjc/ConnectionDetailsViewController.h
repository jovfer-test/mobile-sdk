//
//  ConnectionDetailsViewController.h
//  CMeSdkObjc
//
//  Created by Predrag Jevtic on 6/11/20.
//  Copyright © 2020 Evernym Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConnectionDetailsViewController : UIViewController

@property NSDictionary* connection;

@end

NS_ASSUME_NONNULL_END
