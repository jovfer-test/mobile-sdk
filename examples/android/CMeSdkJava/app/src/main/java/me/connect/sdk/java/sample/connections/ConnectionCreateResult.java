package me.connect.sdk.java.sample.connections;

public enum ConnectionCreateResult {
    SUCCESS,
    REDIRECT,
    FAILURE
}
