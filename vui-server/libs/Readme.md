# Vui Server Dependency Libraries 

Before running Docker image for the first time, please download files listed bellow and copy them to folder **libs**. 

- indy-cli_1.15.0-bionic_amd64.deb
- libindy_1.15.0-bionic_amd64.deb
- libnullpay_1.15.0-bionic_amd64.deb
- libsovtoken_1.0.5_amd64.deb
- libvcx_0.8.72140829-d437603a5-bionic_amd64.deb
- node-vcx-wrapper_0.8.72143220-d437603a5_amd64.tgz

Download links are here: 

https://github.com/evernym/mobile-sdk/releases